var gulp = require('gulp');
var sass = require('gulp-sass');
var postcss = require('gulp-postcss');
var cssnext = require('postcss-cssnext');

var paths = {
  'scss': 'src/css/',
  'css': 'css/'
}

gulp.task('scss', function() {
    var processors = [
        cssnext({
            browsers: 'last 2 versions',
            features:{
              customProperties: true,
              calc: true,
              customMedia: true,
              mediaQueriesRange: true
            }
        })
    ];
    return gulp.src(paths.scss + 'sass/style.scss')
    .pipe(sass())
    .pipe(postcss(processors))
    .pipe(gulp.dest(paths.css))
});

